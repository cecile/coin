# -*- coding: utf-8 -*-

from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.db.models import Q
from django import forms

from .models import Storage
from .validators import validate_future_date

User = get_user_model()


class LoanDeclareForm(forms.Form):
    loan_date_end = forms.DateField(
        label='Date de retour prévue',
        required=False,
        validators=[validate_future_date],
        input_formats=['%d/%m/%Y'],
        help_text='laisser vide si non planifié',
        widget=forms.TextInput(
            attrs={'type': 'date', 'placeholder': 'JJ/MM/AAAA'}))


class LoanReturnForm(forms.Form):
    storage = forms.ModelChoiceField(
        label='Dans quel lieu de stockage ai-je remis le matériel ?',
        required=False,
        queryset=Storage.objects.all(), empty_label='Je ne sais pas')


class LoanTransferForm(forms.Form):
    target_user = forms.CharField(
        max_length=100,
        label='Adhérent',
        help_text='email, pseudonyme ou numéro de l\'adhérent',
    )

    def clean_target_user(self):
        value = self.cleaned_data['target_user']
        result = User.objects.filter(
            Q(email__iexact=value)
            | Q(pk__iexact=value)
            | Q(nickname__iexact=value)
            | Q(username__iexact=value)
        )
        if result.count() > 1:
            raise ValidationError(
                "La recherche retourne plus d'un adhérent")
        elif result.count() < 1:
            raise ValidationError(
                "Aucun adhérent ne correspond à cette recherche")

        return result.first()
