# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin

from coin.configuration.admin import ConfigurationAdminFormMixin
from coin.utils import delete_selected

from .models import HousingConfiguration



class HousingConfigurationInline(admin.StackedInline):
    model = HousingConfiguration
    # fk_name = 'offersubscription'
    readonly_fields = ['configuration_ptr']


class HousingConfigurationAdmin(ConfigurationAdminFormMixin, PolymorphicChildModelAdmin):
    base_model = HousingConfiguration
    list_display = ('offersubscription', 'activated',
                    'ipv4_endpoint', 'ipv6_endpoint', 'comment')
    list_filter = ('activated',)
    search_fields = ('comment',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected, "generate_endpoints", "generate_endpoints_v4",
               "generate_endpoints_v6", "activate", "deactivate")
    inline = HousingConfigurationInline

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return []
        else:
            return []

    def set_activation(self, request, queryset, value):
        count = 0
        # We must update each object individually, because we want to run
        # the save() method to update the backend.
        for housing in queryset:
            if housing.activated != value:
                housing.activated = value
                housing.full_clean()
                housing.save()
                count += 1
        action = "activated" if value else "deactivated"
        msg = "{} Housing subscription(s) {}.".format(count, action)
        self.message_user(request, msg)

    def activate(self, request, queryset):
        self.set_activation(request, queryset, True)
    activate.short_description = "Activate selected Housings"

    def deactivate(self, request, queryset):
        self.set_activation(request, queryset, False)
    deactivate.short_description = "Deactivate selected Housings"

    def generate_endpoints_generic(self, request, queryset, v4=True, v6=True):
        count = 0
        for housing in queryset:
            if housing.generate_endpoints(v4, v6):
                housing.full_clean()
                housing.save()
                count += 1
        msg = "{} Housing subscription(s) updated.".format(count)
        self.message_user(request, msg)

    def generate_endpoints(self, request, queryset):
        self.generate_endpoints_generic(request, queryset)
    generate_endpoints.short_description = "Generate IPv4 and IPv6 endpoints"

    def generate_endpoints_v4(self, request, queryset):
        self.generate_endpoints_generic(request, queryset, v6=False)
    generate_endpoints_v4.short_description = "Generate IPv4 endpoints"

    def generate_endpoints_v6(self, request, queryset):
        self.generate_endpoints_generic(request, queryset, v4=False)
    generate_endpoints_v6.short_description = "Generate IPv6 endpoints"

admin.site.register(HousingConfiguration, HousingConfigurationAdmin)
